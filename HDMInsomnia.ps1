function pause { $null = Read-Host 'Press Enter to exit...' }

$name = 'HDMInsomnia'
$path = "$env:homedrive\$name"
$power_plan_description = " "
$task_description = "Changes power plan when the audio device state is changed."

Write-Host "This will install $name on your computer.`n`nDo you want to continue?"

while ($true)
{
    $input = $(Read-Host '[Y] Yes  [N] No').ToLower()

    if ($input -match 'y(es)?')
    {
        Write-Host "`nInstalling...`n"
        break
    }
    elseif ($input -match 'n(o)?')
    {
        Write-Host "`nAbort.`n"
        pause
        exit
    }
}

$guid_balanced = '381b4222-f694-41f0-9685-ff5bb260df2e'
$guid_new = (powercfg -duplicatescheme $guid_balanced).split(' ')[3]
powercfg -changename $guid_new "$name" "$power_plan_description"
powercfg -setdcvalueindex $guid_new 4f971e89-eebd-4455-a8de-9e59040e7347 5ca83367-6e45-459f-a27b-476b1d01c936 0
powercfg -setacvalueindex $guid_new 4f971e89-eebd-4455-a8de-9e59040e7347 5ca83367-6e45-459f-a27b-476b1d01c936 0

New-Item -Path $path -ItemType Directory | out-null

@"
`$guid_active = (powercfg -getactivescheme).split(' ')[3]
if (`$guid_active -ne '$guid_new')
{
    powercfg /setactive $guid_new
    exit
}
powercfg /setactive $guid_balanced
exit
"@ > "$path\$name.ps1"

@"
CreateObject("WScript.Shell").Run "powershell.exe $path\$name.ps1", 0
"@ > "$path\$name.vbs"

@"
<?xml version="1.0" encoding="UTF-16"?>
<Task version="1.2" xmlns="http://schemas.microsoft.com/windows/2004/02/mit/task">
    <RegistrationInfo>
        <Author>$name</Author>
        <Description>$task_description</Description>
        <URI>\$name</URI>
    </RegistrationInfo>
    <Triggers>
        <EventTrigger>
            <Enabled>true</Enabled>
            <Subscription>&lt;QueryList&gt;&lt;Query Id="0" Path="Microsoft-Windows-Audio/Operational"&gt;&lt;Select Path="Microsoft-Windows-Audio/Operational"&gt;*[System[Provider[@Name='Microsoft-Windows-Audio'] and EventID=65]]&lt;/Select&gt;&lt;/Query&gt;&lt;/QueryList&gt;</Subscription>
        </EventTrigger>
    </Triggers>
    <Principals>
        <Principal id="Author">
            <LogonType>S4U</LogonType>
            <RunLevel>LeastPrivilege</RunLevel>
        </Principal>
    </Principals>
    <Settings>
        <MultipleInstancesPolicy>IgnoreNew</MultipleInstancesPolicy>
        <DisallowStartIfOnBatteries>false</DisallowStartIfOnBatteries>
        <StopIfGoingOnBatteries>false</StopIfGoingOnBatteries>
        <AllowHardTerminate>true</AllowHardTerminate>
        <StartWhenAvailable>true</StartWhenAvailable>
        <RunOnlyIfNetworkAvailable>false</RunOnlyIfNetworkAvailable>
        <IdleSettings>
            <StopOnIdleEnd>true</StopOnIdleEnd>
            <RestartOnIdle>false</RestartOnIdle>
        </IdleSettings>
        <AllowStartOnDemand>true</AllowStartOnDemand>
        <Enabled>true</Enabled>
        <Hidden>false</Hidden>
        <RunOnlyIfIdle>false</RunOnlyIfIdle>
        <WakeToRun>false</WakeToRun>
        <ExecutionTimeLimit>PT72H</ExecutionTimeLimit>
        <Priority>7</Priority>
    </Settings>
    <Actions Context="Author">
        <Exec>
            <Command>wscript.exe</Command>
            <Arguments>$path\$name.vbs</Arguments>
        </Exec>
    </Actions>
</Task>
"@ > "$path\$name.xml"

Register-ScheduledTask -xml (Get-Content "$path\$name.xml" | Out-String) -TaskName $name -User "$env:computername\$env:username" -Force | out-null
Remove-Item "$path\$name.xml"

Write-Host "Done.`n"
pause
exit
