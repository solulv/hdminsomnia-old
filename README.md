[[_TOC_]]

# About
Prevent your laptop from entering sleep mode when the lid is closed, while connected to an external display via HDMI or DisplayPort.

## Background
When I close the laptop lid, I usually want my laptop to enter sleep mode, but there is one exception: when it is connected to an external monitor via HDMI or DisplayPort.

I started digging in Windows' Event Viewer and found an event which seems to indicate that the audio device state has changed.

![](screenshot.png)

## How does it work?
The application consists of a task (in Windows' Task Scheduler), a Virtual Basic script, a PowerShell script and a power plan.

When connecting something via HDMI or DisplayPort, the aforementioned event triggers the task. The task executes the Virtual Basic Script, which is just an intermediary used to silently run the PowerShell script. The PowerShell script changes the current power plan to the aforementioned one, which allows the lid to be closed without entering sleep mode. Disconnecting triggers the same chain of events, but this time the power plan is reset to *Balanced*.

Keep in mind, however, that the event seems to indicate that the audio device state has changed. This means that connecting a Bluetooth speaker, for example, may trigger the task as well.

# Installation
1. Open PowerShell as administrator and run `Set-ExecutionPolicy -ExecutionPolicy Unrestricted` [to allow scripts to run](https://docs.microsoft.com/en-us/previous-versions//bb613481(v=vs.85)?redirectedfrom=MSDN#how-to-allow-scripts-to-run).
2. [Download the PowerShell script](https://gitlab.com/solulv/hdminsomnia/-/raw/main/HDMInsomnia.ps1?inline=false) and run it.

# Uninstallation
1. Delete the task in Task Scheduler.
2. Delete the power plan under Power Options in Control Panel.
3. Delete the folder `C:\HDMInsomnia`.
